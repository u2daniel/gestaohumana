<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cadastro de Dependente</title>
</head>

<body>
<div id="dependente" class="col-lg-12">
    <fieldset>
        <legend class="wbs-legend-form">Dependente</legend>

        <div class="row">
            <form class="form">
                <div class="col-md-8">
                    <div class="form-group  required">
                        <label for="funcionario" class="col-sm-3 control-label">
                            Funcionário
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-7 col-md-8 col-lg-8">
                       <g:select name="funcionario" v-model="dependente.funcionario.id" optionKey="id" from="${br.edu.unirn.Funcionario.list()}" class="form-control"/>
                    </div>
                    </div>

                    <div class="form-group required">
                        <label for="tipo" class="col-sm-3 control-label">
                            Tipo
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-7 col-md-8 col-lg-8">
                    <select name="tipo" v-model="dependente.tipo" id="tipo" class="form-control">
                            <option>Cônjuge</option>
                            <option>Filho(a)</option>
                            <option>Pais</option>
                            <option>Irmã(o)</option>
                            <option>Neto(a)</option>
                            <option>Bisneto(a)</option>
                            <option>Avós</option>
                            <option>Bisavós</option>
                    </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="nome" class="col-sm-3 control-label">
                            Nome
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="nome" v-model="dependente.nome" class="form-control input-sm" required="" id="nome">

                        </div>
                        <label for="dataNascimento" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Dt. Nascimento
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="dataNascimento" v-model="dependente.dataNascimento" class="form-control input-sm date-mask" required="" id="dataNascimento">
                        </div>
                    </div>

                    <div class="form-group required">
                        <input type="button" class="btn btn-info" value="Cadastrar" @click="cadastrar"/>
                    </div>
                </div>
            </form>
        </div>
    </fieldset>
    <fieldset>
        <legend class="wbs-legend-form">Listagem</legend>
        <div class="container">
        <div class="row">
        <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Data de Nascimento</th>
                    <th>Tipo</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody id="myTable">
                <tr v-for="d in dependentes">
                    <td>{{d.nome}}</td>
                    <td>{{d.dataNascimento | formatDate:'dd\/MM\/yyyy'}}</td>
                    <td>{{d.tipo}}</td>
                    <td>
                        <a class="btn btn-primary" @click="editar(d)"><i class="fa fa-edit"></i> </a>
                        <a class="btn btn-danger" @click="remover(d)"><i class="fa fa-remove"></i> </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>

</body>
<asset:javascript src="dependente-controller.js" asset-defer=""/>
<asset:stylesheet src="style.css"/>
</html>