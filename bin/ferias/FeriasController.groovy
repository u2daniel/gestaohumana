package ferias

import br.edu.unirn.Funcionario
import grails.transaction.Transactional
import org.h2.engine.Session
import org.hibernate.SessionFactory

import javax.persistence.Query
import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class FeriasController {

    SessionFactory sessionFactory
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Ferias.list(), model: [feriasCount: Ferias.count()]
    }

    def create() {
        if (params.nome != null) {
            def res = params.nome
            def nome = Funcionario.executeQuery("from Funcionario where nome like '%${res}%'")
            def fer = Ferias.findByFuncionario(nome)
            def parc = Parcelas.findByFerias(fer)

            if(nome.size() == 0 && fer == null){
                respond new Ferias(params), view: 'create', model: [name:nome]
                flash.error = 'Funcionário não existe!!'
            }else{
                respond new Ferias(params), view: 'create', model: [fer: fer,parce:parc, nome: nome]
            }

        } else {

            respond new Ferias(params)
        }
    }

    @Transactional
    def save(Ferias ferias) {
        if (ferias == null) {
            notFound()
            return
        }

        Funcionario res = Funcionario.findByNomeLike("%" + params.nome + "%")

        if (res == null) {
            flash.message = "Funcionário não existe!"
        } else {
            ferias.funcionario = res
        }
       /* def feri_t = Ferias.findByFuncionario(res)
        def prest = Parcelas.findByFerias(feri_t)
        if(prest != null) {
            ferias.inicio = prest.dataSaida
            ferias.fim = prest.dataVolta
        }*/
        ferias.save(flush: true, failOnError: true)

        Parcelas parc = new Parcelas()
        //pega férias funcionário
        def feria = Ferias.findByFuncionario(res)
        def numParc = Integer.parseInt(params.numeroParcelas)
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy")

        if (numParc == 1) {
            def data = formataData(params.data1, params.dia1)
            def data1 = params.data1
            def dataTes = formato.parse(data1)
            parc.dataSaida = dataTes
            parc.dataVolta = data
            parc.dias = Integer.parseInt(params.dia1)
            parc.ferias = feria
            parc.status = "Aguardando Aprovação"
            parc.save(flush: true)

        } else if (numParc == 2) {
            def data = formataData(params.data1, params.dia1)
            def data1 = params.data1
            def dataTes = formato.parse(data1)
            parc.dataSaida = dataTes
            parc.dataVolta = data
            parc.dias = Integer.parseInt(params.dia1)
            parc.ferias = feria
            parc.status = "Aguardando Aprovação"
            parc.save(flush: true)

            Parcelas parce = new Parcelas()
            def data2 = formataData(params.data2, params.dia2)
            def data3 = params.data2
            def dataTesv = formato.parse(data3)
            parce.dataSaida = dataTesv
            parce.dataVolta = data2
            parce.dias = Integer.parseInt(params.dia2)
            parce.ferias = feria
            parce.status = "Aguardando Aprovação"
            parce.save(flush: true)
        } else {
            def data = formataData(params.data1, params.dia1)
            def data1 = params.data1
            def dataTes = formato.parse(data1)
            parc.dataSaida = dataTes
            parc.dataVolta = data
            parc.dias = Integer.parseInt(params.dia1)
            parc.ferias = feria
            parc.status = "Aguardando Aprovação"
            parc.save(flush: true)

            Parcelas parce = new Parcelas()
            def data2 = formataData(params.data2, params.dia2)
            def data3 = params.data2
            def dataTesv = formato.parse(data3)
            parce.dataSaida = dataTesv
            parce.dataVolta = data2
            parce.dias = Integer.parseInt(params.dia2)
            parce.ferias = feria
            parce.status = "Aguardando Aprovação"
            parce.save(flush: true)

            Parcelas parcel = new Parcelas()
            def data4 = formataData(params.data3, params.dia3)
            def dataNes = params.data3
            def dataTest = formato.parse(dataNes)
            parcel.dataSaida = dataTest
            parcel.dataVolta = data4
            parcel.dias = Integer.parseInt(params.dia3)
            parcel.ferias = feria
            parcel.status = "Aguardando Aprovação"
            parcel.save(flush: true)

        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action: "index", method: "GET"
                flash.message= 'Férias cadastradas com sucesso!'
            }
            '*' { respond ferias, [status: CREATED] }
        }
    }

    def formataData(String data, String dia) {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy")
        Date dataFormatada = formato.parse(data)
        def diaNovo = Integer.parseInt(dia)

        Calendar c = Calendar.getInstance()
        c.setTime(dataFormatada)
        c.add(Calendar.DATE, diaNovo)
        def rese = c.getTime()

        return rese

    }

    def edit(Ferias ferias) {
        respond ferias
    }

    @Transactional
    def update(Ferias ferias) {
        if (ferias == null) {
            notFound()
            return
        }

        if (ferias.hasErrors()) {
            respond ferias.errors, view: 'edit'
            return
        }

        ferias.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action: "index", method: "GET"
            }
            '*' { respond ferias, [status: OK] }
        }
    }

    @Transactional
    def delete(Ferias ferias) {

        if (ferias == null) {
            notFound()
            return
        }
        if (new Ferias()?.properties?.containsKey('ativo')) {
            ferias.ativo = false
            ferias.save flush: true
        } else {
            ferias.delete flush: true
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ferias.label', default: 'Ferias'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
