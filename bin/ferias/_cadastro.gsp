<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'ferias.label', default: 'Ferias')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>

<div class="row">

    <div class="col-sm-12">
        <g:if test="${flash.message}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                ${flash.message}
            </div>
        </g:if>
        <g:hasErrors bean="${ferias}">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <i class="glyphicon glyphicon-exclamation-sign"></i> Alguns campos precisam ser corrigidos.
            </div>
        </g:hasErrors>
        <div class="feria">
            <header class="head"> <h4>
                <i class="fa fa-car" aria-hidden="true"></i>  Férias
            </h4>
            </header>
            <div class="linha"></div>
            <div class="feria2">
                <g:form class="form-horizontal" name="feriasForm" action="save">
                    <fieldset class="form">
                        <f:field bean="ferias" property="exercicio"/>
                        <f:field bean="ferias" property="numeroDias"/>
                        <f:field bean="ferias" property="observacoes"/>
                        <f:field bean="ferias" property="justificativa"/>
                        <f:field bean="ferias" property="feriasJudiciais"/>
                        <f:field bean="ferias" property="numeroParcelas"/>
                    </fieldset>
                </g:form>
            </div>
        </div>
        <div class="panel-footer clearfix form-actions"
             style="margin-top: 25px;background-color: #fff; border-radius: 0;">
            <button type="submit" form="feriasForm" class="btn btn-success btn-sm btn-flat btt">
                <i class="glyphicon glyphicon-ok"></i> <g:message code="default.button.create.label"
                                                                  args="[entityName]"/>
            </button>
        </div>
    </div>
    </div>
</body>
html>
