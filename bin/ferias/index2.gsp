<!--
// UNI-RN
// BSI 2017.1
// Técnicas Avançada de Programação
// Docente: Rômulo Fagundes
// Discentes: Marcos Antonio da Silva
//            Daniel Dantas
-->
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main" />
	<title>Cronograma de Férias</title>
</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title">Cronograma de Férias</h1>
		</div>
		<div class="panel-body">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<button type="button" class="btn btn-primary">Solicitações <span class="badge">7</span></button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary">Férias Póximas <span class="badge">2</span></button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-success">Gozando Férias <span class="badge">3</span></button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-danger">Férias Vencendo <span class="badge">5</span></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="page-header">
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-addon">Ano em Exercício </span>
					<input type="text" class="form-control" placeholder="2017">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Alterar</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<div id="divLista" class="col-lg-12">
		<g:render template="lista" model="['ferias':ferias]"></g:render>
	</div>
	<asset:javascript src="application.js" asset-defer=""/>
</body>

</html>