<div class="princDia">
<div class="form-group ${invalid ? 'has-error has-feedback' : required && value ? 'has-success has-feedback' : ''}">
    <label class="col-xs-6 col-sm-4 lab" for="${property}">Número de dias</label>
    <div class="col-sm-9">
        <f:input bean="${bean}" property="${property}" class="inpt4 numeric" placeholder="DIAS"/>
        <g:if test="${invalid}">
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">${errors.join('<br>')}</span>
        </g:if>

    </div>
</div>
</div>
