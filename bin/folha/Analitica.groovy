package folha

import br.edu.unirn.Funcionario

class Analitica {

    Funcionario funcionario
    Evento evento
    String mes
    String ano
    String valor
    String periodo
    String referencia


    static constraints = {

        funcionario()
        evento()
        mes()
        ano()
        valor()
        periodo()
        referencia()
    }
    String toString (){

        funcionario
    }
}
