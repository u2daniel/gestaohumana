package folha


class Evento {

    String codevento
    String descricao
    String tipo
    String baseCalculo
    String formaCalculo
    Date periodo
    Date dateCreated
    Date lastUpdated

    static constraints = {

        codevento()
        descricao()
        tipo()//inList:["Rendimento", "Desconto"]
        baseCalculo()//inList:["Porcetagem", "Fixo"]
        formaCalculo()
        periodo()

    }

    String toString() {
        codevento
    }
}
