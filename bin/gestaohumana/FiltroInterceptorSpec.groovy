package gestaohumana


import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(FiltroInterceptor)
class FiltroInterceptorSpec extends Specification {

    def setup() {
    }

    def cleanup() {

    }

    void "Test filtro interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"filtro")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
