package gestaohumana

import br.edu.unirn.Funcionario

class User {
    String username
    String password
    Boolean ativo
    Funcionario func

    static constraints = {
        username email: true
        password maxSize: 8
        ativo()
    }

    String toString(){
        func
    }
}
