
// UNI-RN
// BSI 2017.1
// Técnicas Avançada de Programação
// Docente: Rômulo Fagundes
// Discente: Marcos Antonio da Silva

function limiteFerias() {
	var moment = require('moment');
	moment().local();
	var dat = "";
	var dtFim = "";
	var dtIniC = "";
	var dtLim = "";
	var dtArr = [];
    var table = document.getElementById('tblFerias'),
    rows = table.getElementsByTagName('tr'), i, j, cells, Fim;

    for (i = 0, j = rows.length; i < j; ++i) {
    	cells = rows[i].getElementsByTagName('td');
    	if (!cells.length) {
    		continue;
    	}
    	dtArr[i] = cells[3].innerHTML;
    }

    for (i = 1, j = rows.length; i < j; ++i) {
    	dtFim = dtArr[i];
    	// Define data para alertar sobre proximidade da data limite
    	// Definido para 3 meses antes da último mês permitido.
    	dtLim = moment(dtFim).add(8, 'month').format();

    	if (moment().isSameOrAfter(moment(dtLim))){
    		// Destaca em vermelho o funcionário se a data
    		// atual for igual ou posterior a data limite.
    		rows[i].style.color = "red";
    	}
    }
}
window.onload = limiteFerias;