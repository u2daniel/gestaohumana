// UNI-RN
// BSI 2017.1
// Técnicas Avançada de Programação
// Docente: Rômulo Fagundes
// Discente: Marcos Antonio da Silva
// Arquivo adaptado a partir do projeto do
// professor Rômulo Fagundes

var app = new Vue({
    el: "#ferias",
    data:{
        ferias: {
            parcelas:{}
        },
        ferias_lista: []
    },
    created: function(){
    	this.listar();
    },
    computed: {
    	formatDate: function(date,fmt){
    		return moment(date).format(fmt);
    	}
    },
    methods: {
    	listar: function(){
    		this.$http.get("/ferias/index.json").then(function(response){
    			this.ferias_lista = response.body;
    		});
    	},
        editar: function (ferias) {
            this.$http.get("/ferias/show/"+ferias.id+".json").then(function(response){
                this.ferias = response.body;
                this.ferias.dataNascimento = moment(this.ferias.dataNascimento).format('DD/MM/YYYY');
                this.ferias.dataAdmissao = moment(this.ferias.dataAdmissao).format('DD/MM/YYYY');
                this.ferias.dataEmissaoRg = moment(this.ferias.dataEmissaoRg).format('DD/MM/YYYY');
            })
        },
        remover: function(ferias){
            var vue = this;
            bootbox.confirm("Deseja remover <b>"+ferias.nome+"</b>?", function(result){
                if(result){
                    vue.$http.delete('/ferias/delete/'+ferias.id).then(function(){
                        this.listar();
                    });
                }
            });
        }
    }
})