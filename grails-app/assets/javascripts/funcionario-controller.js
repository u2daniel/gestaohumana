/**
 * Created by romulofc on 05/04/17.
 */
var app = new Vue({
    el: "#funcionario",
    data: {
        funcionario: {
            funcao: {}
        },
        funcionarios: []
    },
    created: function(){
        this.listar();
    },
    computed: {
        formatDate: function (date,fmt) {
            return moment(date).format(fmt);
        }
    },
    methods: {
        cadastrar: function() {
            var vue = this;
            if (vue.funcionario.id) {
                vue.$http.put('/funcionario/update/'+vue.funcionario.id+'.json',vue.funcionario).then(function(response){
                    vue.funcionario = {funcao:{}};
                    vue.funcionario = {graudeinstrucao:{}};
                    vue.listar();
                });
            } else {
                vue.$http.post('/funcionario/save.json',vue.funcionario).then(function(response){
                    vue.funcionario = {funcao:{}};
                    vue.funcionario = {graudeinstrucao:{}};
                    vue.listar();
                });
            }
        }
        ,
        listar: function(){
            this.$http.get("/funcionario/index.json").then(function(response){
                this.funcionarios = response.body;
            });
        },
        editar: function (funcionario) {
            this.$http.get("/funcionario/show/"+funcionario.id+".json").then(function(response){
                this.funcionario = response.body;
                this.funcionario.dataNascimento = moment(this.funcionario.dataNascimento).format('DD/MM/YYYY');
                this.funcionario.dataAdmissao = moment(this.funcionario.dataAdmissao).format('DD/MM/YYYY');
                this.funcionario.dataEmissaoRg = moment(this.funcionario.dataEmissaoRg).format('DD/MM/YYYY');
            })
        },
        remover: function(funcionario){
            var vue = this;
            bootbox.confirm("Deseja remover <b>"+funcionario.nome+"</b>?", function(result){
                if(result){
                    vue.$http.delete('/funcionario/delete/'+funcionario.id).then(function(){
                        this.listar();
                    });
                }
            });
        },
        incluir: function(funcionario){

        }
    }

});