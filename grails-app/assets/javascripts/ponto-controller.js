//define as entradas do relogio de ponto

var app = new Vue({
    el: "#app-ponto",
    data: {
        regponto: {},
        entradas: [],
        horarioLocal:moment().format('HH:mm:ss')
    },
    created: function(){
        this.listar();
    },
  
    methods: {
        nova_entrada: function() {
            var vue = this;
            if (vue.regponto.id) {
                vue.$http.put('/pontoRelogio/update/'+vue.regponto.id+'.json',vue.regponto).then(function(response){
                    vue.listar();
                    this.regponto = {}
                });
            } else {
                vue.$http.post('/pontoRelogio/save.json',vue.regponto).then(function(response){
                    vue.listar();
                    this.regponto = {}
                });
            }
        }
        ,
        listar: function(){
            this.$http.get("/pontoRelogio/fonte.json").then(function(response){
                this.entradas = response.body;
                
            });
        },
        editar: function (regponto) {
            this.$http.get("/pontoRelogio/show/"+regponto.id+".json").then(function(response){
                this.regponto = response.body;

            })
        },
        remover: function(regponto){
            var vue = this;
            bootbox.confirm("Deseja remover a entrada de ponto: <b>" + regponto.rg_funcionario + " ID " + regponto.id + "</b> ?", function(result){
                if(result){
                    vue.$http.delete('/pontoRelogio/delete/' + regponto.id).then(function(){
                        this.listar();
                    });
                }
            });
        },
        registroPonto:function(){

                     var vue = this;
            if (vue.regponto.id) {
                vue.$http.put('/pontoRelogio/update/'+vue.regponto.id+'.json',vue.regponto).then(function(response){
                    
                    
                });
            } else {
                 
                // vue.regponto.rg_funcionario
                 
                vue.$http.post('/pontoRelogio/save.json',vue.regponto).then(function(response){
                   
                     
                });
            }
        

          

        },
        created(){
        moment.locale('pt-br')
        this.horarioLocal= moment().format('HH:mm:ss');



        window.setInterval(() => {





          this.updateHoraAtual()

        },1000);
    }
            }
});