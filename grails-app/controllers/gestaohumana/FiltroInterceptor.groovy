package gestaohumana


class FiltroInterceptor {

    FiltroInterceptor() {
     /*   match(controller: "ferias", action: "*")*/
    }

    boolean before() {
        if (!session.user) {
            redirect(controller: 'auth', action:'login')
            return false
        }

        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

}

