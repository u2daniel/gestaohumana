package rh.mod.ponto

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RegistroPontoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RegistroPonto.list(), model:[registroPontoCount: RegistroPonto.count()]
    }

    def create() {
        respond new RegistroPonto(params)
    }

    @Transactional
    def save(RegistroPonto registroPonto) {
        if (registroPonto == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (registroPonto.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond registroPonto.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'registroPonto.label', default: 'RegistroPonto'), registroPonto.id])
                redirect action:"index", method:"GET"
            }
            '*' { respond registroPonto, [status: CREATED] }
        }
    }

    def edit(RegistroPonto registroPonto) {
        respond registroPonto
    }

    @Transactional
    def update(RegistroPonto registroPonto) {
        if (registroPonto == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (registroPonto.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond registroPonto.errors, view:'edit'
            return
        }

        registroPonto.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'registroPonto.label', default: 'RegistroPonto'), registroPonto.id])
                redirect action:"index", method:"GET"
            }
            '*'{ respond registroPonto, [status: OK] }
        }
    }

    @Transactional
    def delete(RegistroPonto registroPonto) {

        if (registroPonto == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        if(new RegistroPonto()?.properties?.containsKey('ativo')){
            registroPonto.ativo = false
            registroPonto.save flush:true
        }else{
            registroPonto.delete flush:true
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'registroPonto.label', default: 'RegistroPonto'), registroPonto.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroPonto.label', default: 'RegistroPonto'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
