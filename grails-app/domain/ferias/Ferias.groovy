package ferias

import br.edu.unirn.Funcionario
import grails.databinding.BindingFormat

/*CLASSE FÉRIAS
PATRYCK HERNANDEZ*/

class Ferias {

    Funcionario funcionario
    String exercicio
    Integer numeroDias
    String feriasJudiciais
    String observacoes
    String justificativa
    Integer numeroParcelas

    static constraints = {
        funcionario()
        exercicio nullable: false
        numeroDias nullable: false
        feriasJudiciais nulable: true
        observacoes nullable: true, maxSize: 320, widget: 'textarea'
        justificativa nullable: true, maxSize: 320, widget: 'textarea'
        numeroParcelas nullable: false, maxSize: 3
        status nullable: true
    }
    static hasMany = [status:Status, parc:Parcelas]

    static mapping = {
        status joinTable:[name:"ferias_status", key:"ferias_id", column:'ferias']
    }

}
