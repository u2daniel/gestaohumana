package gestaohumana

class Role {
    String nome
    static constraints = {
        nome nullable: false
    }
    static hasMany = [user:User]
}
