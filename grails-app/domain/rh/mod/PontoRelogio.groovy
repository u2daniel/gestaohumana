package rh.mod

class PontoRelogio {

    String entrada
    String rg_funcionario

    static constraints = {
        entrada blank: true, nullable: true
        rg_funcionario blank: true, nullable: true
    }

     String toString(){
      entrada
    }
}
