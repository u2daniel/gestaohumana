<!--
// UNI-RN
// BSI 2017.1
// Técnicas Avançada de Programação
// Docente: Rômulo Fagundes
// Discente: Marcos Antonio da Silva
-->
<table id="tblFerias">
    <thead>
        <tr>
            <th>Execicio</th>
            <th>Funcionario</th>
            <th>Início</th>
            <th>Fim</th>
            <th>Dias</th>
            <th>Parc.</th>
            <th>Obs.</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        <g:each  var="ferias"in="${feriasLista}">
        <tr style="background-color">
            <td>${ferias.exercicio}</td>
            <td>${ferias.funcionario.nome}</td>
            <td>${ferias.inicio}</td>
            <td>${ferias.fim}</td>
            <td>${ferias.numeroDias}</td>
            <td>${ferias.numeroParcelas}</td>
            <td>${ferias.observacoes}</td>
            <!--
            <td>${formatDate(format:'dd/MM/yyyy',date:ferias.inicio)}</td>
            <td>${formatDate(format:'dd/MM/yyyy',date:ferias.fim)}</td>
            -->
            <td>
                <a class="btn btn-primary" @click="editar(f)"><i class="fa fa-edit"></i> </a>
                <a class="btn btn-danger" @click="remover(f)"><i class="fa fa-remove"></i> </a>
            </td>
        </tr>
    </g:each>
</tbody>
</table>
