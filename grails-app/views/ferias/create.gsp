<%@ page import="ferias.Ferias" %>
<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'ferias.label', default: 'Ferias')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<div class="row">

    <div class="col-sm-12">
        <g:link class="btn btn-default btn-sm btn-flat pull-right" action="index">
            <i class="glyphicon glyphicon-arrow-left"></i> <g:message code="default.back.label" default="voltar"/>
        </g:link>
    </div>

    <div class="col-sm-12">
        <g:if test="${flash.message}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                ${flash.message}
            </div>
        </g:if>
        <g:hasErrors bean="${ferias}">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <i class="glyphicon glyphicon-exclamation-sign"></i> Alguns campos precisam ser corrigidos.
            </div>
        </g:hasErrors>

        <g:if test="${fer != null && parce != null}">
            <div id="tec" class="feria">
            <header class="head"> <h4>
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>  Período Aquisitivo
            </h4>
            </header>
            <div class="table-responsive tabes">
             <table class=" table table-hover">
                 <thead>
                 <tr class="coli">
                    <td><a href="#">EXERCÍCIO</a></td>
                    <td><a href="#">Dias</a></td>
                    <td><a href="#">Início</a></td>
                     <td><a href="#">Fim</a></td>
                     <td><a href="#">Status</a></td>
                </tr>
                 </thead>
            <g:each in="${fer.list()}" var="item">
                <g:each in="${parce.list()}" var="it">
                <tr>
                    <td>${item.exercicio}</td>
                    <td>${it.dias}</td>
                    <td><g:formatDate format="dd/MM/yyyy" date="${it.dataSaida}"/> </td>
                    <td><g:formatDate format="dd/MM/yyyy" date="${it.dataVolta}"/> </td>
                    <td>${it.status}</td>
                </tr>
                </g:each>
            </g:each>
            </table>
            </div>
                <div class="pagination">
                    <g:paginate total="${fers ?: 0}"/>
                </div>
            </div>

        </g:if>

        <g:if test="${nome == null}">
        <div class="feria">
            <header class="head"> <h4>
                <i class="fa fa-car" aria-hidden="true"></i>  Férias
            </h4>
            </header>
            <g:if test="${flash.error}">
                <div class="erros">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ${flash.error}
                </div>
            </g:if>
            <div class="linha"></div>
            <div class="feria2">
        <g:form class="form-horizontal" name="feriasForm1" action="create">
            <fieldset class="forms">
               <label>Nome</label>
                <input name="nome" required class="inpts char"/>
            </fieldset>
        </g:form>
                    <button type="submit" form="feriasForm1" class="btn btn-success btn-sm btn-flat btt1">
                        <i class="glyphicon glyphicon-ok"></i> <g:message code="Buscar"
                                                                          args="[entityName]"/>
                    </button>
                </div>

        </div>
        </g:if>

        <g:if test="${nome != null}">
            <div class="row">

                <div class="col-sm-12">
                    <g:if test="${flash.message}">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            ${flash.message}
                        </div>
                    </g:if>
                    <g:hasErrors bean="${ferias}">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <i class="glyphicon glyphicon-exclamation-sign"></i> Alguns campos precisam ser corrigidos.
                        </div>
                    </g:hasErrors>
                    <div class="feria">
                        <header class="head"> <h4>
                            <i class="fa fa-car" aria-hidden="true"></i>  Férias
                        </h4>
                        </header>
                        <div class="linha"></div>
                        <div class="feria2">
                            <g:form class="form-horizontal" name="feriasForm" action="save">
                                <fieldset class="form">
                                    <g:each in="${nome}" var="nom">
                                        <fieldset class="forms">
                                            <label>Nome</label>
                                            <input type="text" readonly name="nome" class="inpts" value="${nom.nome}"/>
                                        </fieldset>
                                    </g:each>
                                    <f:field bean="ferias" property="exercicio"/>
                                    <f:field bean="ferias" property="numeroDias"/>
                                    <f:field bean="ferias" property="observacoes"/>
                                    <f:field bean="ferias" property="justificativa"/>
                                    <f:field bean="ferias" property="feriasJudiciais"/>
                                    <f:field bean="ferias" property="numeroParcelas"/>
                                    <div class="teste">

                                        <a href="javascript:void(0);" id="bto">Exibir Períodos</a>
                                    </div>
                                    <div id="parc" class="mostPer">
                                        <header class="tit"><h4>
                                            <i class="fa fa-car" aria-hidden="true"></i>  Dados do Parcelamento
                                        </h4>
                                        </header>

                                        <div id="abe" class="diaT">
                                           <div class="loed">
                                               <p>Períodos</p>
                                               <p>Dias</p>
                                               <p>Início</p>
                                               <p>Término</p>
                                           </div>
                                            <label>1º Período</label>
                                            <g:textField class="erf numeric" name="dia1"/>
                                            <g:textField class="dta date" precision="day" name="data1"/>
                                        </div>

                                        <div id="abe2" class="diaT">
                                            <label>2º Período</label>
                                            <g:textField class="erf numeric" name="dia2"/>
                                            <g:textField class="dta date" precision="day" name="data2"/>
                                        </div>

                                        <div id="abe3" class="diaT">
                                            <label>3º Período</label>
                                            <g:textField class="erf numeric" name="dia3"/>
                                            <g:textField class="dta date" precision="day" name="data3"/>
                                        </div>

                                    </div>
                                </fieldset>
                            </g:form>
                        </div>
                    </div>
                    <div class="panel-footer clearfix form-actions"
                         style="margin-top: 25px;background-color: #fff; border-radius: 0;">
                        <button type="submit" form="feriasForm" class="btn btn-success btn-sm btn-flat btt">
                            <i class="glyphicon glyphicon-ok"></i> <g:message code="default.button.create.label"
                                                                              args="[entityName]"/>
                        </button>
                    </div>
                </div>
            </div>
        </g:if>
    </div>
</div>
</body>
<asset:javascript src="application.js" asset-defer=""/>
</html>
