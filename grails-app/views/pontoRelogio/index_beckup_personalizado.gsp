<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cadastro de Função</title>
</head>

<body>
    <div id="app-ponto" class="col-lg-12">
        <fieldset>  
            <legend class="wbs-legend-form">Registro de Ponto</legend>
            <div class="row">
                <form class="form">
                    <div class="col-md-8">
                        <div class="form-group  required">
                            <label for="rgfuncionario" class="col-sm-3 control-label">
                                Registro de Funcionario
                                <span class="required-indicator">*</span>
                            </label>

                            <div class="col-sm-7 col-md-8 col-lg-8">
                                <input type="text" v-model="regponto.rg_funcionario" class="form-control input-sm" name="rgfuncionario" required="" value="" id="rgfuncionario">
                            </div>
                        </div>
                        <div class="form-group  required">
                        <label for="entrada" class="col-sm-3 control-label">
                            Observação
                        </label>
                        <div class="col-sm-7 col-md-8 col-lg-8">
                          <input type="text" v-model="regponto.entrada" class="form-control input-sm" name="entrada" value="" id="entrada">
                        </textarea>
                        </div>
                    </div>
                <div class="form-group required">
                    <input type="button" class="btn btn-info" value="Cadastrar" @click="cadastrar"/>
                </div>
            </div>
        </form>
    </div>
</fieldset>
<fieldset>
    <legend class="wbs-legend-form">Listagem</legend>
    <div class="container">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Registro de Funcionario</th>
                            <th>Registro</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody id="listagemTabela">
                        <tr v-for="ent in entradas">
                            <td>{{ent.rg_funcionario}}</td>
                            <td>{{ent.entrada}}</td>

                            <td>
                                <a class="btn btn-primary" @click="editar(ent)"><i class="fa fa-edit"></i> </a>
                                <a class="btn btn-danger" @click="remover(ent)"><i class="fa fa-remove"></i> </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>

    </body>
    <asset:javascript src="funcao-controller.js" asset-defer=""/>
    <asset:stylesheet src="style.css"/>
    </html>