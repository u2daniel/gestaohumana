package ferias

import grails.test.mixin.*
import spock.lang.*

@TestFor(FeriasController)
@Mock(Ferias)
class FeriasControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null

        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
        assert false, "TODO: Provide a populateValidParams() implementation for this generated test suite"
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.feriasList
            model.feriasCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.ferias!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def ferias = new Ferias()
            ferias.validate()
            controller.save(ferias)

        then:"The create view is rendered again with the correct model"
            model.ferias!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            ferias = new Ferias(params)

            controller.save(ferias)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/ferias/show/1'
            controller.flash.message != null
            Ferias.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def ferias = new Ferias(params)
            controller.show(ferias)

        then:"A model is populated containing the domain instance"
            model.ferias == ferias
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def ferias = new Ferias(params)
            controller.edit(ferias)

        then:"A model is populated containing the domain instance"
            model.ferias == ferias
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/ferias/index'
            flash.message != null

        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def ferias = new Ferias()
            ferias.validate()
            controller.update(ferias)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.ferias == ferias

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            ferias = new Ferias(params).save(flush: true)
            controller.update(ferias)

        then:"A redirect is issued to the show action"
            ferias != null
            response.redirectedUrl == "/ferias/show/$ferias.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/ferias/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def ferias = new Ferias(params).save(flush: true)

        then:"It exists"
            Ferias.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(ferias)

        then:"The instance is deleted"
            Ferias.count() == 0
            response.redirectedUrl == '/ferias/index'
            flash.message != null
    }
}
